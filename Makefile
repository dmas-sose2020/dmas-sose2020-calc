CC := gcc
ODIR := builddir/obj

SRCS := $(wildcard *.c)
BINS := $(SRCS:%.c=%.o)

OBJS = $(patsubst %,$(ODIR)/%,$(_OBJS))

TESTSRC := $(wildcard test/*.c)
TESTBINS := $(TESTSRC:%.c=%.o)

BINS2 := $(filter-out calculator.o, $(BINS))

%.o: %.c
	@echo "Creating object..."
#	${CC} -g -c -o $@ $<
	${CC} -g -c -o $@ $<
  
calculator: $(BINS)
	@echo "Creating executable..."
	${CC} -lm $(BINS) -o calculator.exe

clean:
	@echo "Cleaning directory..."
	rm -f *.o
	find . -type f -name '*.o' -delete

winclean:
	@echo "Cleaning directory..."
	del /s *.o

check: $(TESTBINS)
	@echo "Creating test suite..."
	${CC} -lm $(TESTBINS) $(BINS2) -o test.exe
	@echo "Executing test suite..."
	./test.exe
	