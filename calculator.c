#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "add.h"
#include "max.h"
#include "sub.h"
#include "floor.h"
#include "mul.h"
#include "section.h"
#include "inv.h"
#include "tan.h"
#include "cos.h"
#include "dmas_division.h"
#include "power.h"
#include "exp.h"
#include "mod.h"
#include "sinus.h"

struct cmd_s {
    float (*fun)(float a, float b);    // function with two arguments
    char *name;                 // name of function
    int nr_of_args;             // number of actual arguments (0, 1 or 2)
};

// forward declarations
float myhelp(float a, float b);
float myexit(float a, float b);
float stack_dup(float a, float b);
float stack_swap(float a, float b);
float stack_clear(float a, float b);

struct cmd_s allfuncs[] =
{
    { myhelp, "help", 0 },			// user command for help functionality
    { myexit, "exit", 0 },			// user command for exiting the program
	{ stack_swap, "swap", 0 },		// swap top and second entry on stack
	{ stack_dup, "duplicate", 0 },	// duplicate top entry on stack
	{ stack_clear, "clear", 0 },	// duplicate top entry on stack
    { add, "add", 2 },
    { section, "section", 1},       // section function added by Krishna Satish D S
	{ dmas_max, "max", 2 },
	{ dmas_mod, "mod", 2 },
	{ sub, "sub", 2},
	{ dmas_floor, "floor", 1 },		// floor function added by U. Margull
	{ mul, "mul", 2},
	{dmas_inv,"inv", 1},
	{dmas_tan, "tan", 1},            // tan function added by Ionut Nicolae
    {dmas_cos, "cos", 1},
	{dmas_divide, "div", 2},	// cos function added by Alex Rugina
	{dmas_pow, "pow", 2},    
	{dmas_exp, "exp", 1},		// exponential function added
	{sinus, "sin", 1},
            //inverse function added by G. Hasnain;
               // Multiplication function added by Ratnanav
//	{ my_func, "my function name", 1}, // C function name, user function name (as string), number of used args
    // add here new functions!
    { NULL, "", -1 }
};

struct cmd_s * find_cmd(char*fname)
{
    int i = 0;
    while(allfuncs[i].fun!=NULL) {
        if( strncmp(fname, allfuncs[i].name, strlen(fname))==0) {
            return &allfuncs[i];
        }
        i++;
    }
    return NULL;
};

// helper functions
float myexit(float a, float b)
{
    printf("Bye bye\n");
    exit(0);
    // not reached
    return 0;
}

float myhelp(float a, float b)
{
    int i = 0;
    printf("Supported commands:");
    while(allfuncs[i].fun!=NULL) {
        printf(" %s", allfuncs[i].name );
        i++;
    }
    printf("\n");
    return 0;
}


void error( char* err ) 
{
	printf("Error: %s\n", err );
	exit(1);
}

int isfnumber( char s[], float*fptr)
{
	if( s==0 ) return 0;
	if( isdigit(s[0]) || (s[0]=='.')  || (s[0]=='+')  || (s[0]=='-') ) {
		sscanf( s, "%f", fptr );
		return 1;
	} else {
		return 0;
	}
}

#define STACK_MAX 100
float stack[STACK_MAX];
int stack_idx = 0;
void stack_push( float v )
{
	if( stack_idx < (STACK_MAX-1) ) {
		stack[stack_idx++] = v;
	} else {
		error("stack_push(): Stack overflow");
	}
}

void stack_pop( float * vptr )
{
	if( (stack_idx > 0) && (vptr!=NULL) ) {
		*vptr = stack[--stack_idx];
	} else {
		error("stack_pop(): Stack empty");
	}
}

float stack_dup(float f, float g)
{
	if( stack_idx==0 ) {
		error("stack_dup(): Stack empty");
	} else if( stack_idx < (STACK_MAX-1) ) {
		stack[stack_idx] = stack[stack_idx-1];
		stack_idx++;
	} else {
		error("stack_dup(): Stack overflow");
	}
	return 0.0;
}

float stack_clear(float f, float g)
{
	stack_idx = 0;
	return 0.0;
}
	
float stack_swap(float f, float g)
{
	if( stack_idx<2 ) {
		error("stack_swap(): Stack empty or one element only");
	} else {
		float x = stack[stack_idx-1];
		stack[stack_idx-1] = stack[stack_idx-2];
		stack[stack_idx-2] = x;
	}
	return 0.0;
}

int stack_isempty()
{
	return stack_idx == 0;
}

void stack_print()
{
	printf("[ ");
	for( int i = 0; i<stack_idx; i++ ) 
		printf("%f ", stack[i] );
	printf("]\n");
}

int process_postfix()
{
    char cmd[100];
    struct cmd_s * ptr;
    myhelp(0.0,0.0);
    while(1) {
        float res;
		char* token;
		const char delim[] = " \n";
		int flag_push = 1;
		if( ! stack_isempty() ) {
			stack_print();
		}
        printf("--> ");
        fgets( cmd, 80, stdin );
		token = strtok(cmd, delim );
		while( token != NULL ) {
			float val;
			//printf("token: '%s'\n", token);
			if( isfnumber(token, &val) ) {
				stack_push(val);
			} else {
				ptr = find_cmd(token);
				if( ptr == NULL ) {
					printf("Unknown function name: %s\n", cmd);
					myhelp(0,0);
				} else {
					float a=0, b=0;
					if( ptr->nr_of_args>0 ) {
						if( stack_isempty() ) {
							flag_push = 0;
							printf("arg1: ");
							scanf("%f",&a);							
						} else {
							stack_pop(&a);
						}
						if( ptr->nr_of_args==2 ) {
							if( stack_isempty() ) {
								flag_push = 0;
								printf("arg2: ");
								scanf("%f",&b);							
							} else {
								b = a;
								stack_pop(&a);
							}
						}
					}
					res = ptr->fun(a,b);
					if( ptr->nr_of_args > 0) {
						if( flag_push )
							stack_push( res );
						else 
							printf("Result: %f\n", res);
					}
				}
			}
			token = strtok(NULL, delim);
        }
    }
	return 1;
}

int main(int argc, char *argv[])
{
	printf("DMAS SoSe 2020 calculator V0.2\n");
    printf("new: postfix (polish) notation - try: 3 2 1 add mul\n");
	return process_postfix();
}