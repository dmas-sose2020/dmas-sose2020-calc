#include "section.h"                       // Header file inclusion

float section (float arg1, float arg2) {

    float outValue = 0.0;                  // temp variable to avoid multiple return statements
    if (arg1 < 0) {                        // Condition to check if the input is negative 
    	outValue = (arg1 * (-1));          // Changing the sign 
    } else {
    	outValue = arg1;                   // outputing input value if it is positive
    }
    return outValue;                       // Returing the output 
    
}