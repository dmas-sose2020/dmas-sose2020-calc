#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../mul.h"



// here we test
void test_multiply(CuTest* tc)
{
	float eps = 0.001;
//	CuAssertDblEquals( tc, 0.0, mul(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 12.0, mul(4.0, 3.0), eps );
	CuAssertDblEquals( tc, 1.0, mul(-1.0, -1.0), eps );
	CuAssertDblEquals( tc, 4.0, mul(2.0, 2.0 ), eps );
	CuAssertDblEquals( tc, 1.95, mul(1.5, 1.3), eps );
	CuAssertDblEquals( tc, -4.0, mul(-4.0, 1.0), eps );
}