#include <stddef.h>
#include "CuTest.h"
#include "../sub.h"


void test_sub(CuTest* tc)
{
	float eps = 0.001;
	CuAssertDblEquals( tc, 0.0, sub(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 1.0, sub(1.0, 0.0), eps );
	CuAssertDblEquals( tc, -1.0, sub(0.0, 1.0), eps );
	CuAssertDblEquals( tc, 0.3, sub(1.5, 1.2), eps );
	CuAssertDblEquals( tc, -5.0, sub(0.0, 5.0), eps );
	CuAssertDblEquals( tc, 0.8, sub(2.8, 2.0), eps );
}