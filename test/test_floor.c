#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../floor.h"

// here we test
void test_floor(CuTest* tc)
{
	float eps = 0.001;
//	CuAssertDblEquals( tc, 10.0, dmas_floor(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 0.0, dmas_floor(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 0.0, dmas_floor(0.0001, 0.0), eps );
	CuAssertDblEquals( tc, -1.0, dmas_floor(-0.0001, 0.0), eps );
	CuAssertDblEquals( tc, 3.0, dmas_floor(3.14159, 0.0), eps );
	CuAssertDblEquals( tc, -4.0, dmas_floor(-3.14159, 0.0), eps );
	CuAssertDblEquals( tc, 99999.0, dmas_floor(99999.99, 0.0), eps );
	CuAssertDblEquals( tc, -100000.0, dmas_floor(-99999.5, 0.0), eps );
}