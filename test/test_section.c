#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../section.h"

#define PI 3.14159

// here we test
void test_section(CuTest* tc)
{
	float eps = 0.001;
	CuAssertDblEquals( tc, 0.0, section(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 5, section(5, 0.0), eps );
	CuAssertDblEquals( tc, 9.65443, section(-9.65443, 0.0), eps );
	CuAssertDblEquals( tc, 120304, section(120304, 0.0), eps );
	CuAssertDblEquals( tc, 1, section(1, 0.0), eps );
	CuAssertDblEquals( tc, 1, section(-1, 0.0), eps );
	CuAssertDblEquals( tc, 0, section(-0, 0.0), eps );
}