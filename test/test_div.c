#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../dmas_division.h"

// here we test
void test_div(CuTest* tc)
{
	float eps = 0.0001;
//	CuAssertDblEquals( tc, 10.0, dmas_divide(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 0.0, dmas_divide(0.0, 1), eps );
	CuAssertDblEquals( tc, 5.0, dmas_divide(10, 2), eps );
	CuAssertDblEquals( tc, 1000, dmas_divide(10000, 10), eps );
	CuAssertDblEquals( tc, 1, dmas_divide(3.14159, 3.14159), eps );
	CuAssertDblEquals( tc, 0.001, dmas_divide(1, 1000), eps );
}