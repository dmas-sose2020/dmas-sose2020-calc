#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../cos.h"

#define PI 3.14159

// here we test
void test_cos(CuTest* tc)
{
	float eps = 0.001;
//	CuAssertDblEquals( tc, 10.0, dmas_cos(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 1.0, dmas_cos(0.0, 0.0), eps );
	CuAssertDblEquals( tc, -1.0, dmas_cos(PI, 0.0), eps );
	CuAssertDblEquals( tc, 0.0, dmas_cos(PI/2, 0.0), eps );
	CuAssertDblEquals( tc, 0.0, dmas_cos(1.5*PI, 0.0), eps );
	CuAssertDblEquals( tc, 0.7071, dmas_cos(PI/4, 0.0), eps );
	CuAssertDblEquals( tc, 1.0, dmas_cos(1000*PI, 0.0), eps );
}