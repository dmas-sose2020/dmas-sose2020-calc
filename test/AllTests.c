#include <stdio.h>

#include "CuTest.h"

// declaration of test functions
void test_floor(CuTest*);
void test_cos(CuTest*);
void test_section(CuTest*);
void test_sub(CuTest*);
void test_div(CuTest*);
void test_mod(CuTest*);
void test_exp(CuTest*);
void test_sinus(CuTest*);
void test_tan(CuTest*);
void test_multiply(CuTest*);

CuSuite* CuGetSuite()
{
	CuSuite* suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, test_floor);
	SUITE_ADD_TEST(suite, test_cos);
	SUITE_ADD_TEST(suite, test_section);
	SUITE_ADD_TEST(suite, test_sub);
	SUITE_ADD_TEST(suite, test_div);
	SUITE_ADD_TEST(suite, test_mod);
    SUITE_ADD_TEST(suite, test_exp);
    SUITE_ADD_TEST(suite, test_sinus);
	SUITE_ADD_TEST(suite, test_tan);
    SUITE_ADD_TEST(suite, test_multiply);
	
}

int RunAllTests(void)
{
	CuString *output = CuStringNew();
	CuSuite* suite = CuSuiteNew();

	CuSuiteAddSuite(suite, CuGetSuite());

	CuSuiteRun(suite);
	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);
	return suite->failCount;
}

int main(void)
{
	return RunAllTests();
}
