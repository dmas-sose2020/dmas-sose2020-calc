#include <stddef.h>
#include "CuTest.h"
#include <math.h>
#include<stdio.h>
#include "../tan.h"
#define PI 3.14159
void test_tan(CuTest* tc)
{
	float acceptance_value = 0.089972;
	CuAssertDblEquals( tc, -0.5773502691896257, dmas_tan((5*PI)/6, 0.0),acceptance_value );
	CuAssertDblEquals( tc, -0.3249196962329067, dmas_tan(-1.1*PI, 0.0),acceptance_value );
	CuAssertDblEquals( tc, -1, dmas_tan((3*PI)/4, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 0, dmas_tan(0.0, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 0.3249196962329067, dmas_tan(1.1*PI, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 0.5773502691896257, dmas_tan(PI/6, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 572.9572133543032, dmas_tan(1.5690509975429023, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 1, dmas_tan(PI/4, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 0, dmas_tan(PI, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 197.56996633888815, dmas_tan(PI/2.0064652770036786, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 0, dmas_tan(-9*PI, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 572.9572133542631, dmas_tan( 4.7106436511326955, 0.0), acceptance_value );
	CuAssertDblEquals( tc, 0, dmas_tan(1000*PI, 0.0),acceptance_value );
	CuAssertDblEquals( tc, -197.56996633888815, dmas_tan(-PI/2.0064652770036786, 0.0),acceptance_value );
	CuAssertDblEquals( tc, 0, dmas_tan(9*PI, 0.0),acceptance_value );
}