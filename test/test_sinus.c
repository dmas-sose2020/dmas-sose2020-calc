#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../sinus.h"

#define PI 3.14159265

// here we test
void test_sinus(CuTest* tc)
{
	float eps = 0.001;

	CuAssertDblEquals( tc, 0.0, sinus(0.0), eps );
	CuAssertDblEquals( tc, 0.0, sinus(PI), eps );
	CuAssertDblEquals( tc, 1.0, sinus(PI/2), eps );
	CuAssertDblEquals( tc, -1.0, sinus(1.5*PI), eps );
	CuAssertDblEquals( tc, 0.7071, sinus(PI/4), eps );
	CuAssertDblEquals( tc, 0.0, sinus(1000*PI), eps );
}