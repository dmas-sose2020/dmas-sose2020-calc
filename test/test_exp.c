#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../exp.h"
#define e 2.71828

// here we test
void test_exp(CuTest* tc)
{
	float eps = 0.001;
	CuAssertDblEquals( tc, 1.0, dmas_exp(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 1.6487, dmas_exp(0.5, 0.0), eps );
	CuAssertDblEquals( tc, 0.6065, dmas_exp(-0.5, 0.0), eps );
	CuAssertDblEquals( tc, 1.00941, dmas_exp(0.009, 0.0), eps );
	CuAssertDblEquals( tc, 2.7182, dmas_exp(1, 0.0), eps );
	CuAssertDblEquals( tc, 23.1038, dmas_exp(3.14, 0.0), eps );
	CuAssertDblEquals( tc, 15.1542, dmas_exp(e, 0.0), eps );
}
