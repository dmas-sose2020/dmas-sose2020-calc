#include <stddef.h>

#include "CuTest.h"

// include subject under test

#include "../mod.h"

// here we test
void test_mod(CuTest* tc)
{
	float eps = 0.001;
//	CuAssertDblEquals( tc, 0.0, dmas_floor(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 0.0, dmas_mod(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 78.0, dmas_mod(78.99, 98.99), eps );
	CuAssertDblEquals( tc, 0.0, dmas_mod(-0.0001, 0.0), eps );
	CuAssertDblEquals( tc, -1.0, dmas_mod(34444.22, -1.5), eps );
	CuAssertDblEquals( tc, 0.0, dmas_mod(-34444.22, -1.5), eps );
	CuAssertDblEquals( tc, 1.0, dmas_mod(-34444.22, 1.5), eps );
	CuAssertDblEquals( tc, 0.0, dmas_mod(34444.22, 1.5), eps );
}