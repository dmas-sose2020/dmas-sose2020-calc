#ifndef SECTION_H                         // Inclusion guard start
#define SECTION_H

float section (float arg1, float arg2);   // Function prototype

#endif                                    // Inclusion guard end